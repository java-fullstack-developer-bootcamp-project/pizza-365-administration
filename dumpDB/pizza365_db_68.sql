-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 18, 2022 at 05:23 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365_db_68`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`) VALUES
(1, 'VN', 'Việt Nam'),
(2, 'US', 'Mỹ'),
(3, 'CA', 'Canada'),
(8, 'KR', 'Korea');

-- --------------------------------------------------------

--
-- Table structure for table `drink`
--

CREATE TABLE `drink` (
  `ma_nuoc_uong` varchar(255) NOT NULL,
  `don_gia` int(11) NOT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drink`
--

INSERT INTO `drink` (`ma_nuoc_uong`, `don_gia`, `ghi_chu`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`) VALUES
('COCA', 15000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Cocacola'),
('FANTA', 15000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Fanta'),
('LAVIE', 5000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Lavie'),
('PEPSI', 15000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Pepsi'),
('TRASUA', 40000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Trà sữa trân châu'),
('TRATAC', 10000, NULL, '2022-08-11 06:51:31', '2022-08-11 06:51:31', 'Trà tắc');

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(6);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `size` varchar(255) NOT NULL,
  `don_gia` int(11) DEFAULT NULL,
  `duong_kinh` int(11) DEFAULT NULL,
  `salad` int(11) DEFAULT NULL,
  `so_luong_nuoc_ngot` int(11) DEFAULT NULL,
  `suon` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`size`, `don_gia`, `duong_kinh`, `salad`, `so_luong_nuoc_ngot`, `suon`) VALUES
('L', 250000, 30, 600, 6, 6),
('M', 200000, 25, 300, 3, 4),
('S', 150000, 20, 200, 2, 2),
('XL', 250000, 35, 600, 6, 6),
('XS', 100000, 15, 100, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) DEFAULT NULL,
  `pizza_type` varchar(255) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `voucher_code`, `user_id`) VALUES
(1, '3bH9FDRrCN', 180000, 'M', 'Hải Sản', 180000, '12354', 1),
(2, 'HsTmh54vcM', 150000, 'S', 'Hải Sản', 150000, '12354', 1),
(3, 'xrrsI2X67w', 200000, 'L', 'Hawaii', 200000, '95531', 2),
(4, 'JrRTQzXkSL', 200000, 'L', 'Hải sản', 200000, '95531', 3);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_code`, `region_name`, `country_id`) VALUES
(1, 'CA', 'California', 2),
(2, 'AK', 'Alaska', 2),
(3, 'CT', 'Cần Thơ', 1),
(4, 'HCM', 'Hồ Chí Minh', 1),
(6, 'SE', 'Seoul', 8);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `address`, `email`, `full_name`, `phone`) VALUES
(1, '1 Nguyễn Trãi', 'nga@gmail.com', 'Nga Nguyễn', '0938473832'),
(2, '3 Trần Hưng Đạo', 'nhi@gmail.com', 'Nhi Huỳnh', '09472382381'),
(3, '7 An Bình', 'tin@gmail.com', 'Tín Nguyễn', '09374837282'),
(5, '7 Nguyễn Trãi', 'kiet@gmail.com', 'Kiệt Huỳnh', '092383994');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL,
  `ma_voucher` varchar(255) NOT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `phan_tram_giam_gia` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `ghi_chu`, `ma_voucher`, `ngay_cap_nhat`, `ngay_tao`, `phan_tram_giam_gia`) VALUES
(1, NULL, '12345', NULL, '2022-08-11 07:58:17', '10'),
(2, NULL, '54321', NULL, '2022-08-11 07:58:17', '15'),
(3, NULL, '23456', NULL, '2022-08-11 07:58:17', '20'),
(4, NULL, '34567', NULL, '2022-08-11 07:58:17', '25'),
(5, NULL, '45678', NULL, '2022-08-11 07:58:17', '30'),
(6, NULL, '56789', NULL, NULL, '35'),
(8, 'Tăng phần trăm', '12099', '2022-08-11 10:45:13', NULL, '40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oqixmig4k8qxc8oba3fl4gqkr` (`country_code`);

--
-- Indexes for table `drink`
--
ALTER TABLE `drink`
  ADD PRIMARY KEY (`ma_nuoc_uong`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`size`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dhk2umg8ijjkg4njg6891trit` (`order_code`),
  ADD KEY `FKel9kyl84ego2otj2accfd8mr7` (`user_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_is5udyhip6itt1wt8tj8hx5wq` (`region_code`),
  ADD KEY `FK7vb2cqcnkr9391hfn72louxkq` (`country_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_5mm09tcdn38s2m21c2uwglpmr` (`ma_voucher`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FKel9kyl84ego2otj2accfd8mr7` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FK7vb2cqcnkr9391hfn72louxkq` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
