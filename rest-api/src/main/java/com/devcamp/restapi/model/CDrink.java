package com.devcamp.restapi.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "drink")
public class CDrink {
    @Id
    @NotNull(message = "Phải có mã nước uống")
    @Size(min = 3, message = "Mã nước uống phải có ít nhất 3 ký tự ")
    @Column(name = "ma_nuoc_uong", unique = true)
    private String maNuocUong;

    @NotNull(message = "Phải có tên nước uống")
    @Size(min = 3, message = "Tên nước uống phải có ít nhất 3 ký tự ")
    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;

    @NotNull(message = "Nhập giá tiền")
    @Min(value = 1000, message = "Giá ít nhất từ 1000")
    @Column(name = "don_gia")
    private int donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;


    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "ngay_cap_nhat", nullable = true)
    private Date ngayCapNhat;
    
    public CDrink() {
    }
    public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
}
