package com.devcamp.restapi.model;

import javax.persistence.*;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @Column(name = "size")
	private String size;

    @Column(name = "duong_kinh")
	private int duongKinh;

    @Column(name = "suon")
	private int suon;

    @Column(name = "salad")
	private int salad;

    @Column(name = "so_luong_nuoc_ngot")
	private int soLuongNuocNgot;

    @Column(name = "don_gia")
	private int donGia;
	
	public CMenu() {
    }

    public CMenu(String size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
		this.size = size;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.soLuongNuocNgot = soLuongNuocNgot;
		this.donGia = donGia;
	}

	// method getter and setter
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(int duongKinh) {
		this.duongKinh = duongKinh;
	}

	public int getSuon() {
		return suon;
	}

	public void setSuon(int suon) {
		this.suon = suon;
	}

	public int getSalad() {
		return salad;
	}

	public void setSalad(int salad) {
		this.salad = salad;
	}

	public int getDonGia() {
		return donGia;
	}

	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}

	public int getSoLuongNuocNgot() {
		return soLuongNuocNgot;
	}

	public void setSoLuongNuocNgot(int soLuongNuocNgot) {
		this.soLuongNuocNgot = soLuongNuocNgot;
	}
}
