package com.devcamp.restapi.controller;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.devcamp.restapi.service.*;
import com.devcamp.restapi.model.*;
import com.devcamp.restapi.repository.iUserRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private iUserRepository userRepository;

    // subTask 5: READ - GET all users
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUsers() {
        try {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // subTask 6: READ - GET user by Id
    @GetMapping("/user/{id}")
    public CUser getUserById(@PathVariable Long id) {
        if (userRepository.findById(id).isPresent())
            return userRepository.findById(id).get();
        else
            return null;
    }

    // subTask 7: CREATE - POST new user
    @PostMapping("/user/create")
    public ResponseEntity<Object> createuser(@RequestBody CUser cUser) {
        try {
            return new ResponseEntity<>(userService.createUser(cUser), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "
                    + e.getCause().getCause().getMessage());
            return ResponseEntity
                    .unprocessableEntity()
                    .body("Failed to Create specified Voucher: "
                            + e.getCause().getCause().getMessage());
        }
    }

    // subTask 8: UPDATE - PUT user
    @PutMapping("/user/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Long id, @RequestBody CUser cUser) {
        Optional<CUser> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            CUser updatedUser = userData.get();
            updatedUser.setFullname(cUser.getFullname());
            updatedUser.setEmail(cUser.getEmail());
            updatedUser.setPhone(cUser.getPhone());
            updatedUser.setAddress(cUser.getAddress());
            //updatedUser.setOrders(cUser.getOrders());
            CUser savedUser = userRepository.save(updatedUser);
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // subTask 9: DELETE - Delete a user
    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
        try {
            Optional<CUser> optional = userRepository.findById(id);
            if (optional.isPresent()) {
                userRepository.deleteById(id);
            } else {
                // UserRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
