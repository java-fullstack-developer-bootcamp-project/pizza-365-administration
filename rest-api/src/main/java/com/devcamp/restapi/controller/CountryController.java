package com.devcamp.restapi.controller;

import java.util.*;
import org.springframework.web.bind.annotation.*;
import com.devcamp.restapi.service.*;
import com.devcamp.restapi.model.*;
import com.devcamp.restapi.repository.iCountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
    @Autowired
    private CountryService countryService;

    @Autowired
	private iCountryRepository countryRepository;
    // subTask 5: READ - GET all countries
    @GetMapping("/countries")
	public ResponseEntity<List<CCountry>> getAllCountries() {
		try {
			return new ResponseEntity<>(countryService.getAllCountries(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    // subTask 6: READ - GET country by Id
    @GetMapping("/country/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

    //subTask 7: CREATE - POST new country
    @PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			return new ResponseEntity<>(countryService.createCountry(cCountry), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " 
            + e.getCause().getCause().getMessage());
			return ResponseEntity
            .unprocessableEntity()
            .body("Failed to Create specified Voucher: " 
            + e.getCause().getCause().getMessage());
		}
	}

    //subTask 8: UPDATE - PUT country
    @PutMapping("/country/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CCountry drink = countryData.get();
				drink.setCountryCode(cCountry.getCountryCode());
                drink.setCountryName(cCountry.getCountryName());
				return new ResponseEntity<>(countryRepository.save(drink), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    // subTask 9: DELETE - Delete a country
    @DeleteMapping("/country/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}
