package com.devcamp.restapi.controller;
import com.devcamp.restapi.model.*;
import com.devcamp.restapi.repository.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class OrderController {
    @Autowired
	private iOrderRepository orderRepository;

	@Autowired
	private iUserRepository userRepository;

	// subTask 10: READ - GET all Orders
	@GetMapping("/orders")
	public List<COrder> getAllOrder() {
		return orderRepository.findAll();
	}

	// subTask 11: READ - GET order by user id
	@GetMapping("/user/{userId}/orders")
	public List<COrder> getOrdersByUserId(@PathVariable(value = "userId") Long userId) {
		return orderRepository.findByUserId(userId);
	}

	// subTask 12: READ - GET order by id
	@GetMapping("/order/{id}")
	public COrder getOrderByOrderId(@PathVariable Long id) {
		if (orderRepository.findById(id).isPresent()) {
			return orderRepository.findById(id).get();
		} else {
			return null;
		}
	}

	// subTask 13: CREATE - POST new Order
	@PostMapping("/order/create/{id}")
	public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
		try {
			Optional<CUser> userData = userRepository.findById(id);
			if (userData.isPresent()) {
				COrder newRole = new COrder();
				newRole.setOrderCode(cOrder.getOrderCode());
				newRole.setPizzaSize(cOrder.getPizzaSize());
				newRole.setPizzaType(cOrder.getPizzaType());
				newRole.setPaid(cOrder.getPaid());
				newRole.setPrice(cOrder.getPrice());
				newRole.setVoucherCode(cOrder.getVoucherCode());

				CUser _user = userData.get();
				newRole.setUser(_user);

				COrder savedRole = orderRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "
					+ e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// subTask 14: UPDATE - PUT an order
	@CrossOrigin
	@PutMapping("/order/update/{id}")
	public ResponseEntity<Object> updateOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
		Optional<COrder> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			COrder updatedOrder = orderData.get();
			updatedOrder.setPizzaSize(cOrder.getPizzaSize());
			updatedOrder.setPizzaType(cOrder.getPizzaType());
			updatedOrder.setPaid(cOrder.getPaid());
			updatedOrder.setPrice(cOrder.getPrice());
			updatedOrder.setVoucherCode(cOrder.getVoucherCode());
			COrder savedOrder = orderRepository.save(updatedOrder);
			return new ResponseEntity<>(savedOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// subTask 15: DELETE - Delete an order
    @CrossOrigin
	@DeleteMapping("/order/delete/{id}")
	public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
