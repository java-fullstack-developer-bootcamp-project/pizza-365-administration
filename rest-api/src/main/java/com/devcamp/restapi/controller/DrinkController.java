package com.devcamp.restapi.controller;

import java.util.*;

import javax.validation.Valid;

import com.devcamp.restapi.model.CDrink;
import com.devcamp.restapi.repository.iDrinkRepository;
import com.devcamp.restapi.service.DrinkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DrinkController {
    @Autowired
	iDrinkRepository pDrinkRepository;
	@Autowired
	DrinkService pDrinkService;

	// subTask 5 READ
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllDrinksBySevice() {
		try {
			return new ResponseEntity<>(pDrinkService.getAllDrinks(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// subTask 6: READ by maNuocUong
	@GetMapping("/drink/{maNuocUong}")
	public ResponseEntity<CDrink> getCDrinkBymaNuocUong(@PathVariable("maNuocUong") String maNuocUong) {
		Optional<CDrink> drinkData = pDrinkRepository.findBymaNuocUong(maNuocUong);
		if (drinkData.isPresent()) {
			return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// subTask 7 CREATE
	@PostMapping("/drink/create") // Dùng phương thức POST
	public ResponseEntity<Object> createCDrink(@Valid @RequestBody CDrink pDrinks) {
		try {
 
			Optional<CDrink> drinkData = pDrinkRepository.findBymaNuocUong(pDrinks.getMaNuocUong());
			if (drinkData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
			}
			pDrinks.setNgayTao(new Date());
			pDrinks.setNgayCapNhat(null);
			CDrink _Drinks = pDrinkRepository.save(pDrinks);
			return new ResponseEntity<>(_Drinks, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
		}
	}

	// subTask 8 UPDATE
	@PutMapping("/drink/update/{maNuocUong}") // Dùng phương thức PUT
	public ResponseEntity<CDrink> updateCDrinkBymaNuocUong(@PathVariable("maNuocUong") String maNuocUong,
			@RequestBody CDrink pDrinks) {
		try {
			Optional<CDrink> drinkData = pDrinkRepository.findBymaNuocUong(maNuocUong);
			if (drinkData.isPresent()) {
				CDrink drink = drinkData.get();
				drink.setDonGia(pDrinks.getDonGia());
				drink.setGhiChu(pDrinks.getGhiChu());
				drink.setNgayCapNhat(new Date());
				return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	//subTask 9 DELETE
	@DeleteMapping("/drink/delete/{maNuocUong}")// Dùng phương thức DELETE
	public ResponseEntity<CDrink> deleteCDrinkBymaNuocUong(@PathVariable("maNuocUong") String maNuocUong) {
		try {
			pDrinkRepository.deleteCDrinkBymaNuocUong(maNuocUong);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
