package com.devcamp.restapi.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.restapi.model.CVoucher;
import com.devcamp.restapi.repository.iVoucherRepository;
import com.devcamp.restapi.service.VoucherService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class VoucherController {
    @Autowired 
    private VoucherService voucherService;

    @Autowired
    iVoucherRepository pVoucherRepository;

    // subTask 5: getAllVouchers()
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            return new ResponseEntity<>(voucherService.getAllVouchers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/voucher/{id}")
    public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
        try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
            if (voucherData.isPresent()) {
                return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // subTask 7: CREATE
    @PostMapping("/voucher/create")
    public ResponseEntity<Object> createCVoucher(@Valid @RequestBody CVoucher pVouchers) {
        try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
            if(voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}
            pVouchers.setNgayTao(new Date());
            pVouchers.setNgayCapNhat(null);

            CVoucher _vouchers = pVoucherRepository.save(pVouchers);

            return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  

    // subTask 8: UPDATE
    @PutMapping("/voucher/update/{id}")
    public ResponseEntity<CVoucher> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
        try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
            if (voucherData.isPresent()) {
                CVoucher voucher = voucherData.get();
                voucher.setMaVoucher(pVouchers.getMaVoucher());
                voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
                voucher.setGhiChu(pVouchers.getGhiChu());
                voucher.setNgayCapNhat(new Date());
                return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // subTask 9: DELETE
    @DeleteMapping("/voucher/delete/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
