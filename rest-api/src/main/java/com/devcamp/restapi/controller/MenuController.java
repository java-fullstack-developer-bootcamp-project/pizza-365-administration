package com.devcamp.restapi.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.restapi.model.CMenu;
import com.devcamp.restapi.repository.iMenuRepository;
import com.devcamp.restapi.service.MenuService;

@Transactional
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class MenuController {
     @Autowired 
    private MenuService menuService;

    @Autowired
    private iMenuRepository menuRepository;

    // READ - GET all menu
    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getMenus() {
        try {
            return new ResponseEntity<>(menuService.getAllMenus(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // READ - Get menu by size
    @GetMapping("/menu/{size}")
    public ResponseEntity<Object> getMenuBySize(@PathVariable("size") String size) {
        Optional<CMenu> menuData = menuRepository.findBySize(size);
        if (menuData.isPresent()) {
            return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }


    // CREATE - POST new menu
    @PostMapping("/menu/create")
    public ResponseEntity<Object> createuser(@RequestBody CMenu cMenu) {
        try {
            return new ResponseEntity<>(menuService.createMenu(cMenu), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "
                    + e.getCause().getMessage());
            return ResponseEntity
                    .unprocessableEntity()
                    .body("Failed to Create specified Menu: "
                            + e.getCause().getCause().getMessage());
        }
    }    
    // UPDATE - PUT an existing menu
    @PutMapping("/menu/update/{size}")
    public ResponseEntity<Object> updateUser(@PathVariable("size") String size, @RequestBody CMenu cMenu) {
        Optional<CMenu> menuData = menuRepository.findBySize(size);
        if (menuData.isPresent()) {
            CMenu updatedMenu = menuData.get();
            updatedMenu.setDonGia(cMenu.getDonGia());
            updatedMenu.setDuongKinh(cMenu.getDuongKinh());
            updatedMenu.setSalad(cMenu.getSalad());
            updatedMenu.setSoLuongNuocNgot(cMenu.getSoLuongNuocNgot());
            updatedMenu.setSuon(cMenu.getSuon());
            CMenu savedMenu = menuRepository.save(updatedMenu);
            return new ResponseEntity<>(savedMenu, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    // DELETE - Delete an existing menu
    @DeleteMapping("/menu/delete/{size}")
    public ResponseEntity<Object> deleteMenuBySize(@PathVariable("size") String size) {
        try {
            menuRepository.deleteBySize(size);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
