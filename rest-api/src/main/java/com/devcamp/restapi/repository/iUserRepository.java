package com.devcamp.restapi.repository;
import org.springframework.data.repository.CrudRepository;

import com.devcamp.restapi.model.CUser;
public interface iUserRepository extends CrudRepository<CUser, Long> {
     
}
