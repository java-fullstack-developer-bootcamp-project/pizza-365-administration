package com.devcamp.restapi.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.restapi.model.COrder;

@Repository
public interface iOrderRepository extends JpaRepository<COrder, Long> {
	List<COrder> findByUserId(long userId);
    
}
