package com.devcamp.restapi.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CDrink;
@Transactional
public interface iDrinkRepository extends JpaRepository<CDrink, Long> {

    Optional<CDrink> findBymaNuocUong(String maNuocUong);

    void deleteCDrinkBymaNuocUong(String maNuocUong);
}
