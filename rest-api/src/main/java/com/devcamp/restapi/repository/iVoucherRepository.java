package com.devcamp.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CVoucher;

public interface iVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
