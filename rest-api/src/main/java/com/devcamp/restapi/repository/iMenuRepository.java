package com.devcamp.restapi.repository;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.model.CMenu;

public interface iMenuRepository extends JpaRepository<CMenu, Long> {

    Optional<CMenu> findBySize(String size);

    void deleteBySize(String size);
}
