package com.devcamp.restapi.repository;
import com.devcamp.restapi.model.CCountry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface iCountryRepository extends JpaRepository<CCountry, Long> {
    CCountry findByCountryCode(String countryCode);
}
