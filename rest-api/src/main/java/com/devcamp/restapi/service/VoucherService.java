package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.CVoucher;
import com.devcamp.restapi.repository.iVoucherRepository;

@Service
public class VoucherService {
    @Autowired
    iVoucherRepository pVoucherRepository;

    public ArrayList<CVoucher> getAllVouchers() {
        ArrayList<CVoucher> listVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listVoucher::add);
        return listVoucher;
    }
}
