package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.CDrink;
import com.devcamp.restapi.repository.iDrinkRepository;

@Service
public class DrinkService {
    @Autowired
    iDrinkRepository pDrinkRepository;

    public ArrayList<CDrink> getAllDrinks() {
        ArrayList<CDrink> listDrink = new ArrayList<>();
        pDrinkRepository.findAll().forEach(listDrink::add);
        return listDrink;
    }
}
