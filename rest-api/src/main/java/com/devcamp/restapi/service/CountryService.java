package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.CCountry;
import com.devcamp.restapi.repository.iCountryRepository;

@Service
public class CountryService {
    @Autowired
    iCountryRepository pCountryRepository;

    public ArrayList<CCountry> getAllCountries() {
        ArrayList<CCountry> listCountry = new ArrayList<>();
        pCountryRepository.findAll().forEach(listCountry::add);
        return listCountry;
    }

    public CCountry createCountry(CCountry cCountry) {
        CCountry country = new CCountry();
        country.setCountryName(cCountry.getCountryName());
        country.setCountryCode(cCountry.getCountryCode());
        country.setRegions(cCountry.getRegions());
        CCountry savedCountry = pCountryRepository.save(country);
        return savedCountry;
    }
}
