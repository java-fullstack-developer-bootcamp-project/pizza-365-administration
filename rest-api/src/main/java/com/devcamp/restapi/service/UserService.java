package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.CUser;
import com.devcamp.restapi.repository.iUserRepository;

@Service
public class UserService {
    @Autowired
    iUserRepository pUserRepository;

    public ArrayList<CUser> getAllUsers() {
        ArrayList<CUser> listUser = new ArrayList<>();
        pUserRepository.findAll().forEach(listUser::add);
        return listUser;
    }

    public CUser createUser(CUser cUser) {
        CUser user = new CUser();
        user.setFullname(cUser.getFullname());
        user.setEmail(cUser.getEmail());
        user.setPhone(cUser.getPhone());
        user.setAddress(cUser.getAddress());
        user.setOrders(cUser.getOrders());
        CUser savedUser = pUserRepository.save(user);
        return savedUser;
    }
}
