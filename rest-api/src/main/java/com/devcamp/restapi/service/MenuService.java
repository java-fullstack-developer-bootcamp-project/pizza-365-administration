package com.devcamp.restapi.service;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.model.CMenu;
import com.devcamp.restapi.repository.iMenuRepository;

@Service
public class MenuService {
    @Autowired
    iMenuRepository pMenuRepository;

    public ArrayList<CMenu> getAllMenus() {
        ArrayList<CMenu> listMenu = new ArrayList<>();
        pMenuRepository.findAll().forEach(listMenu::add);
        return listMenu;
    }

    public CMenu createMenu(CMenu cMenu) {
        CMenu menu = new CMenu();
        menu.setSize(cMenu.getSize());
        menu.setDonGia(cMenu.getDonGia());
        menu.setDuongKinh(cMenu.getDuongKinh());
        menu.setSalad(cMenu.getSalad());
        menu.setSoLuongNuocNgot(cMenu.getSoLuongNuocNgot());
        menu.setSuon(cMenu.getSuon());
        CMenu savedMenu = pMenuRepository.save(menu);
        return savedMenu;
    }
}
